
jtwitter.jar - Twitter client jars
twitter4j-2.0.9.jar - Twitter client jars
jopt-simple-3.1.jar - Command-line option parser
commons-logging.jar -  Logging
commons-math-2.0.jar - For score calculations
junit-4.4.jar - Testing
log4j-1.2.15.jar - Logging
rome-1.0.jar - Java RSS 
jdom-1.1.jar - for rome
spring-beans.jar - Basic Spring jar
spring-context.jar - ibid
spring-core.jar - ibid
