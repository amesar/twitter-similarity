package com.andre.twitter;

import java.util.*;
import org.apache.log4j.Logger;
import org.apache.commons.math.stat.correlation.PearsonsCorrelation;

/**
 * Similarity formulas.
 */
public class Formulas {
	private static final Logger log = Logger.getLogger(Formulas.class);
	private static final PearsonsCorrelation pearsons = new PearsonsCorrelation();

	/** Calculate Euclidean distance. */

	public static double calcDistance(Map<String,Integer> words1, Map<String,Integer> words2) {
		Set<String> set = intersection(words1, words2);
		if (set.size() == 0) return 0;

		double sum = 0 ;
		for (String key : set)
			sum += Math.pow(words1.get(key)-words2.get(key),2);

		return 1.0 / (1.0 + sum);
	}

	/** Calculate Pearsons distance. */

	public static double calcPearsons(Map<String,Integer> words1, Map<String,Integer> words2) {
		Set<String> common = intersection(words1, words2);
		int n = common.size();
		if (n == 0) return 0;

		double [] d1 = new double[n];
		double [] d2 = new double[n];
		int j=0;
		for (String key : common) {
			d1[j] = words1.get(key);
			d2[j] = words2.get(key);
			j++;
		}
		double score = pearsons.correlation(d1,d2);
		return score;
	}

	private static <K,V> Set<K> intersection(Map<K,V> map1, Map<K,V> map2) {
		Set<K> set = new HashSet<K>();
		for (Map.Entry<K,V> entry : map1.entrySet()) {
			K key = entry.getKey() ;
			if (map2.get(key) != null)
				set.add(key);
		}
		return set ;
	}

	/** Calculate Tanimoto distance. */

	public static <T> double calcTanimoto(Set<T> set1, Set<T> set2) {
		Set<T> common = new HashSet<T>(set1);
		common.retainAll(set2);
		double csize = (double) common.size();
		return csize / ( (double)set1.size() + (double)set2.size() - csize);
	}

	public static <K,V> double calcTanimoto(Map<K,V> map1, Map<K,V> map2) {
		Set<K> common = intersection(map1, map2);
		double csize = (double) common.size();
		return csize / ( (double)map1.size() + (double)map2.size() - csize);
	}
}
