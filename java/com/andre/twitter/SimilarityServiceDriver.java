package com.andre.twitter;

import java.util.*;
import org.apache.log4j.Logger;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.context.ApplicationContext;
import joptsimple.OptionParser;
import joptsimple.OptionSet;

/**
 * Main driver for Twitter SimilarityService
 */
public class SimilarityServiceDriver {
	private static final Logger log = Logger.getLogger(SimilarityServiceDriver.class);

	private String [] configFilenames = {"config.xml" };
	private String beanName = "similarityService" ;
	private SimilarityService service;
	private String user1 ;
	private String user2 ;

	public static void main(String[] args) throws Exception {
		SimilarityServiceDriver pgm = new SimilarityServiceDriver();
		pgm.process(args) ;
	}

	public void process(String[] args) throws Exception {
		initOptions(args);
		initSpring();
		doit(args);
	}

	private void doit(String [] args) throws Exception {
		double score = service.getSimilarityScore(user1,user2);
		info("User1="+user1);
		info("User2="+user2);
		info("Score="+score);
	}

	@SuppressWarnings("unchecked")
	private void initSpring() {
		info("Service="+beanName);
		ApplicationContext context = new ClassPathXmlApplicationContext(configFilenames);
		service = (SimilarityService)context.getBean(beanName);
	}

	private static final String OPT_SERVICE = "service" ;

	private void initOptions(String [] args) {
		OptionParser parser = new OptionParser();
		parser.accepts(OPT_SERVICE,	"SimilarityService bean name").withRequiredArg();
		OptionSet options = parser.parse(args);
		if (options.has(OPT_SERVICE))
			beanName = (String)options.valueOf(OPT_SERVICE);

		List<String> nonOptions = options.nonOptionArguments();
		if (nonOptions.size() < 2) {
			log.error("User1 and User2 are required");
			throw new RuntimeException("Missing user1 and user2");
		}
		user1 = nonOptions.get(0);
		user2 = nonOptions.get(1);
	}

	private static void info(Object o) { System.out.println(o) ; }
}
