package com.andre.twitter;

import java.io.*;
import java.util.*;
import java.nio.charset.Charset;
import com.google.common.io.Files;
import com.google.common.base.Charsets;

/**
 * IO utilities.
 */
public class IoUtils {
	public static final Charset CHARSET = Charsets.ISO_8859_1 ;

	public static String readFile(File file) throws IOException {
		return Files.toString(file, CHARSET);
	}

	public static void writeFile(File file, String content) throws IOException {
		Files.write(content.getBytes(), file);
	}
}
