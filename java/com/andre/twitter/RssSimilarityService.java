package com.andre.twitter;

import java.io.*;
import java.util.*;
import java.net.URL;
import com.sun.syndication.feed.synd.SyndFeed;
import com.sun.syndication.io.SyndFeedInput;
import com.sun.syndication.io.XmlReader;
import com.sun.syndication.feed.synd.SyndEntry;
import com.sun.syndication.feed.synd.SyndContent;
import org.apache.log4j.Logger;

/**
 * RSS implementation of SimilarityService.
 */
public class RssSimilarityService extends AbstractSimilarityService {
	private static final Logger log = Logger.getLogger(RssSimilarityService.class);

	public RssSimilarityService(SimilarityScorer scorer, WordCounter wordCounter) {
		super(scorer, wordCounter);
	}

	@Override
	public double getSimilarityScore(String feedUrl1, String feedUrl2) throws Exception {
		String text1 = getFeedText(feedUrl1);
		String text2 = getFeedText(feedUrl2);
		log.debug("feedUrl1: "+feedUrl1);
		log.debug("feedUrl2: "+feedUrl2);
		double score = calculateSimilarityScore(text1, text2);
		return score ;
	}

	@SuppressWarnings("unchecked")
	private String getFeedText(String feedUrl) throws Exception {
		URL url = new URL(feedUrl);
		SyndFeedInput input = new SyndFeedInput();
		SyndFeed feed = input.build(new XmlReader(url));

		List<SyndEntry> list = feed.getEntries();
		StringBuilder buf = new StringBuilder();
		for (SyndEntry entry : list) {
			if (useDescription) {
				SyndContent desc = entry.getDescription();
				buf.append(desc.getValue()+" ");
			}
			if (useContent) {
				List<SyndContent> clist = entry.getContents();
				for (SyndContent content : clist)
					buf.append(content.getValue()+" ");
			}
		}
		return buf.toString();
	}
 
	private boolean useContent = false ; 
	public boolean getUseContent() { return useContent; }
	public void setUseContent(boolean useContent) { this.useContent = useContent; } 
 
	private boolean useDescription = true ; 
	public boolean getUseDescription() { return useDescription; }
	public void setUseDescription(boolean useDescription) { this.useDescription = useDescription; } 
}
