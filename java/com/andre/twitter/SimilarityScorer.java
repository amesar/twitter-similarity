package com.andre.twitter;

import java.util.Map;

/**
 * Calculates similarity score for two word scords.
 */
public interface SimilarityScorer {
	public double calculateSimilarityScore(Map<String,Integer> wordScores1, Map<String,Integer> wordScores2);
}
