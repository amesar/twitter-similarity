package com.andre.twitter;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Miscellaneous utilities.
 */
public class MiscUtils {
	private static final String pattern = "yyyy-MM-dd_HH.mm.ss" ;
	private static final DateFormat fmt = new SimpleDateFormat(pattern);

	public static String formatDate() {
		return formatDate(new Date());
	}

	public static String formatDate(Date date) {
		return fmt.format(date);
	}
}
