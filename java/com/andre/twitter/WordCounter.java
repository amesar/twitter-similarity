package com.andre.twitter;

import java.util.*;
import java.io.*;
import java.net.URLDecoder;

/**
 * Counts words.
 */
public class WordCounter {
	//private Set<String> stopPrefixes = new HashSet<String>();
	private Set<String> stopWords = new HashSet<String>();
	private String splitPattern = "[\\s.,?!;:]+" ;

	public WordCounter() {
	}

	public WordCounter(File stopWordsFile) throws IOException {
		BufferedReader reader = null;
		try {
			reader = new BufferedReader(new FileReader(stopWordsFile));
			StringBuilder buf = new StringBuilder();
			String line;
			for (;;) {
				line = reader.readLine();
				if (line==null) break;
				if (!line.startsWith("#"))	// comment
					stopWords.add(line.toLowerCase());
			}
		} finally {
			if (reader != null) reader.close();
		}
	}

	public Map<String,Integer> countWords(String text) {
		Map<String,Integer> map = new HashMap<String,Integer>();
		String [] words = text.split(splitPattern);
		for (String word : words) {
			if (word.trim().length() == 0) continue ;
			word = word.toLowerCase();
			if (word.startsWith("http")) continue ;
			if (stopWords.contains(word)) continue ;
			Integer count = map.get(word);
			if (count == null)
				count = 0;
			map.put(word,count+1);
		}
		return map ;
	}

	public void print(Map<String,Integer> map) {
		print(map, null);
	}

	public void print(Map<String,Integer> map, String msg) {
		msg = msg==null ? "" : " - "+msg;
		info("Word Counts"+msg+" #words="+map.size());
		for (Map.Entry<String,Integer> entry : map.entrySet()) {
			info("	"+entry.getKey()+"="+entry.getValue());
		}
	}

	public String getSplitPattern() { return splitPattern; }
	public void setSplitPattern(String splitPattern) { this.splitPattern = splitPattern; } 

	static void info(Object o) { System.out.println(o);}
}
