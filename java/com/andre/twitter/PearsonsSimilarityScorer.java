package com.andre.twitter;

import java.util.Map;

/**
 * Calculates similarity score using Pearson's correlation.
 */
public class PearsonsSimilarityScorer implements SimilarityScorer {
	public double calculateSimilarityScore(Map<String,Integer> wordScores1, Map<String,Integer> wordScores2) {
		return Formulas.calcPearsons(wordScores1,wordScores2);
	}
}
