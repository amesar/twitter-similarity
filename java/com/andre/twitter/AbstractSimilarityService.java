package com.andre.twitter;

import java.util.*;
import java.io.File;
import java.io.IOException;
import org.apache.log4j.Logger;

/**
 * Abstract class for SimilarityService.
 */
abstract public class AbstractSimilarityService implements SimilarityService {
	private static final Logger log = Logger.getLogger(AbstractSimilarityService.class);
	private final WordCounter wordCounter;
	private final SimilarityScorer scorer ;

	public AbstractSimilarityService(SimilarityScorer scorer, WordCounter wordCounter) {
		this.wordCounter = wordCounter;
		this.scorer = scorer;
	}

	double calculateSimilarityScore(String text1, String text2) {
		Map<String,Integer> map1 = wordCounter.countWords(text1) ;
		Map<String,Integer> map2 = wordCounter.countWords(text2) ;
	
		log.debug("text1: "+text1);
		log.debug("text2: "+text2);
		if (log.isDebugEnabled()) {
			wordCounter.print(map1,"map1");
			wordCounter.print(map2,"map2");
		}
		double score = scorer.calculateSimilarityScore(map1, map2);
		return score ;
	}

	void writeLogFile(String user, Date date, String text) throws IOException
	{
		if (writeLogFile) {
			//String filename = "log-user-"+user+".txt" ;
			String filename = "log-user-"+user+"_"+MiscUtils.formatDate(date)+".txt" ;
			IoUtils.writeFile(new File(filename), text);
		} 
	}

	private boolean writeLogFile = true ;
	public boolean getWriteLogFile() { return writeLogFile; }
	public void setWriteLogFile(boolean writeLogFile) { this.writeLogFile = writeLogFile; }
}
