package com.andre.twitter;

import java.util.*;
import twitter4j.Status;
import twitter4j.TwitterException;

/**
 * Twitter4j utilities.
 */
public class Twitter4jUtils {
	public static void print(List<Status> statuses, String user, int count) throws TwitterException {
		info("---------------------");
		info("UserTimeline: user=" + user+" count="+count);
		info("	#Status="+statuses.size());
		int j=0;
		for (Status status : statuses) {
			info("	 Status "+j+" :");
			info("		 Text: "+status.getText());
			info("		 User.Name: "+status.getUser().getName());
			j++;
		}
	}

	private static void info(Object o) { System.out.println(o);}
}

