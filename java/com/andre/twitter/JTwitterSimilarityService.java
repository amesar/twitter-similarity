package com.andre.twitter;

import java.io.*;
import java.util.*;
import org.apache.log4j.Logger;
import winterwell.jtwitter.Twitter;

/**
 * JTwitter implementation of SimilarityService.
 */
public class JTwitterSimilarityService extends AbstractSimilarityService {
	private static final Logger log = Logger.getLogger(JTwitterSimilarityService.class);
	private final Twitter service ;
	private final String username ;

	public JTwitterSimilarityService(SimilarityScorer scorer, WordCounter wordCounter, String username, String password) throws Exception {
		super(scorer, wordCounter);
		this.username = username;
		service = new Twitter(username, password);
	}

	@Override public double getSimilarityScore(String user1, String user2) throws Exception {
		Date date = new Date();
		return calculateSimilarityScore(getText(user1, date),getText(user2, date));
	}

	private String getText(String user, Date date) throws Exception {
		List<Twitter.Status> statutes = service.getPublicTimeline();

		StringBuilder sbuf = new StringBuilder();
		List<Twitter.Status> statuses = service.getUserTimeline(user);

		for (Twitter.Status status : statuses) {
			sbuf.append(status.getText()+" ");
		}

		String text = sbuf.toString();
		writeLogFile(user, date, text);
		log.debug("Text: "+text);
		return text;
	}
}
