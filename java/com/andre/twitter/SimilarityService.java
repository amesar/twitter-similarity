package com.andre.twitter;

/**
 * Similarity Service.
 */
public interface SimilarityService {
	public double getSimilarityScore(String user1, String user2) 
		throws Exception ;
}
