package com.andre.twitter;

import java.io.*;
import java.util.*;

/**
 * Mock file-based implementation of SimilarityService. Reads plain text files for each user.
 */
public class MockSimilarityService extends AbstractSimilarityService {
	private final File dir ;

	public MockSimilarityService(SimilarityScorer scorer, WordCounter wordCounter, File dir) {
		super(scorer, wordCounter);
		this.dir = dir ;
	}

	@Override
	public double getSimilarityScore(String user1, String user2) throws Exception {
		String text1 = IoUtils.readFile(new File(dir, user1));
		String text2 = IoUtils.readFile(new File(dir, user2));
		return calculateSimilarityScore(text1, text2);
	}
}
