package com.andre.twitter;

import java.io.*;
import java.util.*;
import org.apache.log4j.Logger;
import twitter4j.Twitter;
import twitter4j.Status;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.Paging;
import twitter4j.conf.ConfigurationBuilder;

/**
 * Twitter4j implementation of SimilarityService.
 */
public class Twitter4jSimilarityService extends AbstractSimilarityService {
	private static final Logger log = Logger.getLogger(Twitter4jSimilarityService.class);
	private final Twitter service ;
	private int count = 20 ;
	private int pageNumber = 1 ;

	public Twitter4jSimilarityService(SimilarityScorer scorer, WordCounter wordCounter) throws Exception {
		super(scorer, wordCounter);
		service = new TwitterFactory().getInstance();
	}

	@Override
	public double getSimilarityScore(String user1, String user2) throws Exception {
		Date date = new Date();
		return calculateSimilarityScore(getText(user1, date),getText(user2, date));
	}

	private String getText(String user, Date date) throws TwitterException, IOException {
		Paging paging = new Paging(pageNumber,count);
		List<Status> statuses = service.getUserTimeline(user, paging); 

		if (log.isDebugEnabled())
			Twitter4jUtils.print(statuses,user,count);

		StringBuilder sbuf = new StringBuilder();
		for (Status status : statuses) 
			sbuf.append(status.getText()+" ");

		String text = sbuf.toString();
		writeLogFile(user, date, text);
		return text;
	}

	public int getCount() { return count; }
	public void setCount(int count) { this.count = count; } 

	private static void info(Object o) { System.out.println(""+o);}
}
