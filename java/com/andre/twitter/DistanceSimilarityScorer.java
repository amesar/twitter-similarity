package com.andre.twitter;

import java.util.Map;

/**
 * Calculates similarity score using Euclidian distance.
 */
public class DistanceSimilarityScorer implements SimilarityScorer {
	public double calculateSimilarityScore(Map<String,Integer> wordScores1, Map<String,Integer> wordScores2) {
		return Formulas.calcDistance(wordScores1,wordScores2);
	}
}
