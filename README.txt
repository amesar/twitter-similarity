*******************************************************************************
*
* README.txt - Twitter User Similarity                             06 may 2010
*
*******************************************************************************

To run

  First change the credentials in config/config.properties:
    change properties cfg.user and cfg.password to reflect your Twitter 
    credentials 

  If you do not have ant installed, you can run the scripts listed below.
  If you want to build the Java files and run the tests, type: ant.
  This will:
    - build lib/user-similarity.jar
    - run JUnit tests and leave output in build/junit/index.html

Scripts
  run-jtwitter.sh - Runs JTwitter provider
  run-twitter4j.sh - Runs TWitter4j provider
  run-mock.sh - Runs mock file provider - files are in data/
  run-rss.sh - Scores output of two RSS feeds

Ant targets: ant -p
 all      clean,compile,test  (default target)
 clean    Cleans the world
 compile  Compile Java files
 test     Run JUnit tests

Output of run-twitter4j.sh
  Service=twitter4jSimilarityService
  User1=springrod
  User2=cbeust
  Score=0.08333333333333333

Directories
  java - java source code
  config - configuration files
  data - sample data files
  lib - 3rd party jar files
  doc - documentation

Tests
  build/junit/index.html
